const {body} = require('express-validator');
export default {
  validateRegister: [
    body('email').isEmail().withMessage('The email is invalid format'),
  ],
};
