var mongoose = require('mongoose')

var schema = new mongoose.Schema({ 
    name: 'string',
    email: 'string', 
    password: 'string',
    phone: 'string',
    address: 'string', 
});
module.exports = mongoose.model('User', schema);