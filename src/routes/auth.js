const express = require('express');
const router = express.Router();

import validator from '../middleware/validator';
import authValidation from '../validation/auth';
import authController from '../controllers/auth';

// khi call api từ postman nó sẽ đi vào validation trước để check format, xong sẽ vào validator để xem có lỗi format nào k, có thì response lỗi về, k có thì vào tiếp controller
router.post('/register', authValidation.validateRegister, validator, authController.register);


router.get('/', authController.list);

router.get('/search', authController.search);

router.put('/profile', authController.profile);

router.post('/login', authController.login);
module.exports = router;
