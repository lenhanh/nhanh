const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/amazon', {useNewUrlParser: true})
    .then(()=>{
      console.log('MongoDB is connected');
    })
    .catch((e)=>{
      console.log('MongoDB: ', e);
    });
