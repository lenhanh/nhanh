const User = require('../models/user.js');
const bcrypt = require('bcrypt');
const saltRounds = 10;

const register = (req, res, next) => {
  const {email, password, name, phone, address} = req.body;
  const hash = bcrypt.hashSync(password, saltRounds);
  const user = new User({name, email, 'password': hash, phone, address}); // viet gon lai nhu ri
  user.save((err, item)=>{
    if (err) {
      res.send('Can\'t register');
    } else {
      res.send('The account is registered');
    }
  });
};

const list = (req, res, next) => {
  User.find({}, (err, items)=>{
    res.send(items);
  });
};

const search = (req, res, next) => {
  User.find({'address': 'quang nam'}, (err, items)=>{
    if (err) {
      res.send('no address');
    } else {
      res.send(items);
    }
  });
};

const profile = (req, res, next) => {
  const {email, name, phone, address} = req.body;
  User.findOne({'email': email}, (err, item)=>{
    if (item==null) {
      res.send('The account is not exsited');
    } else {
      User.updateOne({email: email}, {$set: {name: name, phone: phone, address: address}}, (err)=>{
        res.send('Updated');
      });
    }
  });
};

const login = (req, res, next) => {
  const {email, password} = req.body;
  User.findOne({'email': email}, (err, item)=>{
    if (item==null) {
      res.send('The account is not exsited');
    } else {
      if (bcrypt.compareSync(password, item.password)) {
        res.send('hello '+item.name);
      } else {
        res.send('The account is not exsited');
      }
    }
  });
};

export default {
  register,
  list,
  search,
  profile,
  login,
};
